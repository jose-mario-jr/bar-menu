module Model.Person exposing
    ( Person
    , decoder
    , empty
    , encoder
    )

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)
import Json.Encode as Encode


type alias Person =
    { id : Int, name : String }


empty : Person
empty =
    { id = -1, name = "" }


decoder : Decoder Person
decoder =
    Decode.succeed Person
        |> required "id" Decode.int
        |> required "name" Decode.string


encoder : Person -> Encode.Value
encoder person =
    Encode.object
        [ ( "id", Encode.int person.id )
        , ( "name", Encode.string person.name )
        ]
