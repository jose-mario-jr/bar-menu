module Model.Selection exposing
    ( Selection
    , decoder
    , encoder
    , totalPrice
    , viewAll
    )

import Html exposing (Html, button, div, text)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)
import Json.Encode as Encode
import Model.Item as Item exposing (Item)
import Utils exposing (formatMoney)


type alias Selection =
    { item : Item
    , quantity : Int
    }


decoder : Decoder Selection
decoder =
    Decode.succeed Selection
        |> required "item" Item.decoder
        |> required "quantity" Decode.int


encoder : { item : Item, quantity : Int } -> Encode.Value
encoder body =
    Encode.object
        [ ( "item", Item.encoder body.item )
        , ( "quantity", Encode.int body.quantity )
        ]


viewAll : List Selection -> (String -> msg) -> Html msg
viewAll selection onclick =
    let
        listItems =
            selection
                |> List.filter (\l -> l.quantity > 0)
                |> List.map (view onclick)
    in
    if listItems == [] then
        div [ class "box" ] [ text "Nenhum item selecionado" ]

    else
        div [ class "box selections" ]
            [ div []
                [ div [ class "title" ] [ text "Selected Items:" ]
                , div [ class "items" ] listItems
                , div [ class "totalPrice" ] [ text "Total Price: ", totalPrice selection ]
                ]
            ]


view : (String -> msg) -> Selection -> Html msg
view onclick l =
    button
        [ class "item", onClick (onclick l.item.name) ]
        [ div [ class "name" ] [ text l.item.name ]
        , div [ class "side-by-side" ]
            [ div [ class "val" ] [ text (formatMoney l.item.price) ]
            , div [ class "val" ] [ text (String.fromInt l.quantity) ]
            ]
        ]


totalPrice : List Selection -> Html msg
totalPrice selection =
    selection
        |> List.map
            (\l ->
                l.item.price * toFloat l.quantity
            )
        |> List.sum
        |> formatMoney
        |> text
