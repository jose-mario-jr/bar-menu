module WrapperStyled exposing (styledViewWrapper)

import Css
    exposing
        ( alignItems
        , backgroundColor
        , borderRadius4
        , bottom
        , center
        , color
        , displayFlex
        , fixed
        , height
        , hex
        , justifyContent
        , padding2
        , pct
        , position
        , px
        , top
        , width
        )
import Css.Global
import Html.Styled exposing (Attribute, Html, a, div, footer, header, styled, text)
import Html.Styled.Attributes exposing (href)
import Route


styledHeader : List (Attribute a) -> List (Html a) -> Html a
styledHeader =
    styled Html.Styled.header
        [ position fixed
        , height (px 50)
        , top (px 0)
        , width (pct 100)
        , backgroundColor (hex "#001800")
        , displayFlex
        , alignItems center
        , justifyContent center
        , borderRadius4 (px 0) (px 0) (px 10) (px 10)
        ]


styledFooter : List (Attribute a) -> List (Html a) -> Html a
styledFooter =
    styled Html.Styled.header
        [ position fixed
        , height (px 50)
        , bottom (px 0)
        , width (pct 100)
        , backgroundColor (hex "#001800")
        , displayFlex
        , alignItems center
        , justifyContent center
        , borderRadius4 (px 10) (px 10) (px 0) (px 0)
        ]


styledApp : List (Attribute a) -> List (Html a) -> Html a
styledApp =
    styled Html.Styled.div
        [ width (pct 100)
        , padding2 (px 50) (px 0)
        ]


globalStyleNode : Html msg
globalStyleNode =
    Css.Global.global
        [ Css.Global.html
            [ height (pct 100)
            ]
        , Css.Global.body
            [ backgroundColor (hex "#1f1f1f")
            ]
        ]


styledViewWrapper : List (Attribute a) -> List (Html a) -> Html a
styledViewWrapper attrs rest =
    styled Html.Styled.div
        []
        []
        [ styledApp attrs rest
        , globalStyleNode
        , styledHeader [] [ text "header" ]
        , styledFooter []
            [ text "footer"
            , a [ href (Route.toString Route.ListTabs) ] [ text "Goto home" ]
            ]
        ]
