module Pages.ListTabs exposing (Model, Msg(..), init, update, view)

import Base exposing (Base, base)
import Browser exposing (Document)
import Html exposing (h1, p, text)
import Model.Tab as Tab exposing (Tab)
import RemoteData exposing (RemoteData(..), WebData, fromResult)
import Url exposing (Protocol(..))


type alias Model =
    { tabs : WebData (List Tab)
    }


init : a -> ( Model, Cmd Msg )
init _ =
    ( initialModel, Tab.fetchAll (fromResult >> TabsReceived) )


initialModel : Model
initialModel =
    { tabs = Loading
    }


type Msg
    = TabsReceived (WebData (List Tab))


view : Model -> Document Msg
view model =
    { title = "List Tabs"
    , body =
        base
            (Base
                []
                [ h1 [] [ text "List Tabs" ]
                , p [] [ text "This is a list of tabs." ]
                , Base.fromWebData model.tabs Tab.viewAll
                ]
                []
            )
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        TabsReceived response ->
            ( { model | tabs = response }, Cmd.none )
