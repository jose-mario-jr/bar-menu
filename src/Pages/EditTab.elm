module Pages.EditTab exposing
    ( Model
    , Msg(..)
    , init
    , update
    , view
    )

import Base exposing (base)
import Browser exposing (Document)
import Browser.Navigation as Nav
import Error exposing (buildErrorMessage)
import Html exposing (Html, a, button, div, span, text)
import Html.Attributes exposing (class, href)
import Html.Events exposing (onClick)
import Http
import Model.Category as Category exposing (Category)
import Model.Person exposing (Person)
import Model.Selection as Selection exposing (Selection)
import Model.Tab as Tab exposing (Tab)
import Route


type alias Model =
    { bar : Person
    , categories : List Category
    , navKey : Nav.Key
    , tab : Tab
    , readyState : State
    }


type State
    = Loading
    | Success
    | Error String


init : Int -> Nav.Key -> ( Model, Cmd Msg )
init flags navKey =
    ( initialModel flags navKey, Category.fetchAll CategoriesReceived )


initialModel : Int -> Nav.Key -> Model
initialModel clientId navKey =
    { bar = Person 5258 "Bar do Ze"
    , tab = Tab clientId (Person 0 "") []
    , navKey = navKey
    , categories = []
    , readyState = Loading
    }


type Msg
    = AddItem String
    | RemoveItem String
    | CategoriesReceived (Result Http.Error (List Category))
    | TabReceived (Result Http.Error Tab)
    | SaveTab
    | TabSaved (Result Http.Error Tab)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AddItem item ->
            let
                newSelection =
                    if List.any (\i -> i.item.name == item) model.tab.selection then
                        List.map
                            (\l ->
                                if l.item.name == item then
                                    { l | quantity = l.quantity + 1 }

                                else
                                    l
                            )
                            model.tab.selection

                    else
                        model.categories
                            |> List.concatMap (\l -> l.items)
                            |> List.filter (\i -> i.name == item)
                            |> List.map (\i -> Selection i 1)
                            |> (++) model.tab.selection
                            |> List.sortBy (\i -> i.item.name)

                oldTab =
                    model.tab

                newTab =
                    { oldTab | selection = newSelection }
            in
            ( { model | tab = newTab }, Cmd.none )

        RemoveItem item ->
            let
                newSelection =
                    model.tab.selection
                        |> List.map
                            (\l ->
                                if l.item.name == item then
                                    { l | quantity = l.quantity - 1 }

                                else
                                    l
                            )
                        |> List.filter (\l -> l.quantity > 0)

                oldTab =
                    model.tab

                newTab =
                    { oldTab | selection = newSelection }
            in
            ( { model | tab = newTab }, Cmd.none )

        CategoriesReceived (Ok response) ->
            ( { model | categories = response, readyState = Success }, Tab.fetch model.tab.id TabReceived )

        CategoriesReceived (Err e) ->
            ( { model | readyState = Error (buildErrorMessage e) }, Cmd.none )

        TabReceived (Ok response) ->
            ( { model | tab = response, readyState = Success }, Cmd.none )

        TabReceived (Err e) ->
            let
                errMsg =
                    "TabFetch: " ++ buildErrorMessage e
            in
            ( { model | readyState = Error errMsg }, Cmd.none )

        SaveTab ->
            ( { model | readyState = Loading }, Tab.save model.tab TabSaved )

        TabSaved (Ok _) ->
            ( { model | readyState = Success }, Route.pushUrl Route.ListTabs model.navKey )

        TabSaved (Err error) ->
            ( { model | readyState = Error (buildErrorMessage error) }, Cmd.none )


view : Model -> Document Msg
view model =
    { title =
        "Comanda #" ++ String.fromInt model.tab.client.id
    , body =
        getBody model
    }


getBody : Model -> List (Html Msg)
getBody model =
    case model.readyState of
        Loading ->
            [ div [ class "loading" ] [ text "Loading..." ] ]

        Error e ->
            [ div [ class "error" ] [ text ("Error: " ++ e) ] ]

        Success ->
            base (getApp model)


getApp model =
    { head =
        [ div [ class "left" ]
            [ a
                [ class "nav", href "/" ]
                [ span [] [ text "<" ] ]
            , div
                []
                [ text ("Local: " ++ model.bar.name) ]
            ]
        , div
            []
            [ text ("Client: " ++ model.tab.client.name) ]
        ]
    , body =
        [ Category.viewAll model.categories AddItem
        , Selection.viewAll model.tab.selection RemoveItem
        ]
    , feet =
        [ div [ class "save" ]
            [ div [ class "items" ]
                [ button
                    [ class "item", onClick SaveTab ]
                    [ text "Save" ]
                ]
            ]
        ]
    }
